# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

ECARGO_FEATURES=( ffi full )

require cargo rust
# need the source tarballs from github, the ones on crates.io don't
# contain the source code needed for running the test suite
require github [ user=hyperium tag=v${PV} ]

SUMMARY="A fast and correct HTTP implementation for Rust"

HOMEPAGE="https://hyper.rs/ ${HOMEPAGE}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

ECARGO_FEATURES=( ffi full )

pkg_setup() {
    default

    export RUSTFLAGS="${RUSTFLAGS} --cfg hyper_unstable_ffi"
    # need this one too, or rustdoc will error out
    export RUSTDOCFLAGS="${RUSTDOCFLAGS} --cfg hyper_unstable_ffi"
}

src_install() {
    dolib "${WORK}"/target/$(rust_target_arch_name)/release/lib${PN}.so

    insinto /usr/$(exhost --target)/include/hyper
    doins "${WORK}"/capi/include/hyper.h

    insinto /usr/$(exhost --target)/lib/pkgconfig
    hereins ${PN}.pc << EOF
prefix=/usr
exec_prefix=\${prefix}/$(exhost --target)
libdir=\${exec_prefix}/lib
includedir=\${exec_prefix}/include

Name: hyper
Description: A fast and correct HTTP library.
Version: ${PV}
Libs: -L\${libdir} -lhyper
Cflags: -I\${includedir}/hyper
Libs.private: -lpthread -lm -ldl -lc
EOF

    eautomagic
}

src_test() {
    esandbox allow_net --connect "inet:127.0.0.1@1024-65535"

    cargo_src_test

    esandbox disallow_net --connect "inet:127.0.0.1@1024-65535"
}

