# Copyright 2016-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ disable_default_features=true rust_minimum_version=1.56.0 ] \
    systemd-service [ systemd_files=[ contrib/librespot.service ] systemd_user_files=[ contrib/librespot.user.service ] ]

SUMMARY="librespot is an open source client library for Spotify"
DESCRIPTION="
librespot enables applications to use Spotify's service, without using the official but
closed-source libspotify. Additionally, it will provide extra features which are not available in
the official library. librespot only works with a Spotify Premium enabled account.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa
    gstreamer
    pulseaudio
"

# Doesn't run any tests, last checked: 0.1.3
RESTRICT="test"

DEPENDENCIES="
    build+run:
        group/librespot
        user/librespot
        alsa? ( sys-sound/alsa-lib )
        gstreamer? (
            dev-libs/glib:2
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        pulseaudio? ( media-sound/pulseaudio )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/librespot-systemd-service.patch
)

ECARGO_FEATURE_ENABLES=(
    'alsa alsa-backend'
    'gstreamer gstreamer-backend'
    'pulseaudio pulseaudio-backend'
)

src_install() {
    cargo_src_install

    install_systemd_files

    insinto /etc/conf.d
    hereins librespot.conf <<EOF
LIBRESPOT_ARGS=""
EOF
}

